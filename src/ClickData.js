import React, { useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SocialLogin from './SocialLogin';
import LogoutScreen from './LogoutScreen';
import { compose } from 'recompose';
import { inject, observer } from 'mobx-react';
import { Grid, Paper, Button } from '@material-ui/core';
import FilterData from './FilterData';

    const ClickData = ()=>{
        const handler =() =>  {
             if(isLoggedIn){
                setNavigateToFilter(true);
             }else{
                toast.error("Sorry Please Login First With Google OR Facebook");
             }
        };
        const [isLoggedIn, setIsLoggedIn] = useState(false);       
        const [ navigateToFilter, setNavigateToFilter ] = useState(false);
    return (
        <>
            {!navigateToFilter 
             ? <Grid
                    container
                    spacing={0}
                    direction="column"
                    alignItems="center"
                    justify="center"
                    style={{ minHeight: '100vh' }}
                >
                    <Grid item xs={3}>
                        <Paper elevation={3}>
                            <div style={{ padding: "15px 15px 15px 15px" }}>
                            {
                                isLoggedIn ? 
                                <LogoutScreen isLoggedIn={setIsLoggedIn} /> : 
                                <SocialLogin isLoggedIn={setIsLoggedIn} />
                            }
                            <ToastContainer />
                                <div className="button">
                                    <Button color="secondary" onClick={handler}>Data </Button>
                                </div>
                            </div>            
                        </Paper>
                    </Grid>   
                </Grid>
                :
                <FilterData setIsLoggedIn={setIsLoggedIn} setNavigateToFilter={setNavigateToFilter} />}
        </>
           );
        }
        export default compose(
            inject(({
                userDataStore : { getUserData }
            }
            ) => ({
                getUserData,
            })),
            observer
        )(ClickData);