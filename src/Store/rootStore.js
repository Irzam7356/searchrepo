
import UserDataStore from "./Store";

const Stores = {
  userDataStore: UserDataStore
};

export default () => {
  const stores = {};

  Object.keys(Stores).forEach(key => {
    stores[key] = new Stores[key](stores);
  });

  Object.values(stores).forEach(store => store.init());

  return stores;
};
