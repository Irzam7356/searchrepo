import {observable, action  } from 'mobx';

class Store {
    init = () => {};
    @observable userData = {}

    @action setUserData = payload => {
        localStorage.setItem('userData', JSON.stringify(payload));
        this.userData = payload;
        console.log("setUserData userData ", this.userData);
    }
    @action removeUserData = () => {
        localStorage.removeItem('userData')
        console.log("After remove DAta" , this.userData)
    }
    get getUserData(){
        return this.userData;
    }
}
export default Store;