import React from 'react';
import _ from "lodash";
import { DEGREES, YEARS } from "./data";
import ResultData from './ResultData';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import CardActions from '@material-ui/core/CardActions';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Grid ,Button, AppBar, Toolbar, Typography } from '@material-ui/core';
import { Fragment } from 'react';
import { mergeArray } from './Utils/utils';

 class FilterData extends React.Component{
  constructor(props) {
    super(props);
        this.state ={ names: DEGREES,  Degree: '', year: '' ,error: null, isLoaded: false};
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleReset = this.handleReset.bind(this)
      } 
       async componentDidMount() {
         try{
        // const io = ("socket.io")();
        // const socket = io("http://localhost:8000/getNameAndDate");
        // socket.io('connection', ()=>{
        //     console.log("connection")
        //  })
          setInterval(async () => {
            axios.get("http://localhost:8000/getNameAndDate")
            .then(
              (timeResult) => {
                this.setState({
                  isLoaded: true,
                  data: timeResult.data
                });
              },
              (error) => {
                this.setState({
                  isLoaded: true,
                  error
                });
              }
            )
          },1000);
         } catch (error){
           console.log(error);
         }
      }
      handleSubmit(event){
        try{
        event.preventDefault();
        var degreeResult, yearResult;
        const { Degree, year } = this.state;
        const qualifications = this.state.names.qualifications;
        if(!Degree && !year){
          return;
        }
        if(Degree){
          degreeResult = _.filter(qualifications,function(item){ 
            if(item.degree.includes(Degree.toUpperCase()))
            return item; 
          })
        }
        if(year){
          yearResult = _.filter(qualifications, o => o.year === year);
        }
        if(yearResult){
          console.log("yearResult ",yearResult)
        }
        if(degreeResult){
          console.log("degreeResult ",degreeResult)
        }
        const finalResult = mergeArray(yearResult, degreeResult)
        console.log("mergeArray ", finalResult)
        this.setState({
          result: finalResult
        })
      }catch(error){
        console.log("Error",error)
      }
      }
      handleChange(event){
      try {
          this.setState({ [event.target.name] : event.target.value })
      } catch (error) {
        console.log("Error",error)
      }
      }
      handleReset()
      {
      try {
        this.setState({ result : [] , Degree : '', year : '' });
      } catch (error) {
        console.log("Error",error)
      } 
      }
    render(){
        const { error, isLoaded } = this.state;
        if (error) {
          return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
          return <div style={{
            marginLeft: '50%',
            marginTop: '20%'
          }}><CircularProgress color="secondary" /></div>;
        } else {
        return(
            <>
            <Grid container
            spacing={3}
            direction="column"
            alignItems="center"
            justify="center"
            style={{ minHeight: '50vh' }}
        >
        <form style={{
          top: "50%",
          position: "sticky"
        }} >
        <AppBar>
          <Toolbar>
          <ArrowBackIcon onClick={() => {
            this.props.setIsLoggedIn(true);
            this.props.setNavigateToFilter(false);
          }} />
          <Typography variant="h6" style={{
              marginInline: 'auto'
            }}>{ this.state.data }</Typography>
          </Toolbar>
        </AppBar>
        <Fragment>
        <Grid 
        >
              <label htmlFor='degree'> <b>Degree :</b> </label>
              <input 
                name='Degree'
                placeholder='Search' 
                value = {this.state.Degree}
                onChange={this.handleChange}
              />    
            <label htmlFor='Year'> <b>Year :</b>  </label>
            <select
             name='year'
             value = {this.state.year}
             onChange={this.handleChange} >
            {YEARS.map( (year) =>(
              <option key={year} value={year}>{year}</option>
                 ))}
            </select>
            </Grid>
            <Grid >
                {
                  this.state.result?.length 
                    ? <ResultData result= {this.state.result}  /> 
                    : <p> Not found </p>
                }
            </Grid>
            <Grid >
            <CardActions style={{justifyContent: 'center'}}>
            <Button variant="outlined"  type="submit" value="Submit" color="primary" onClick={this.handleSubmit}>Submit </Button>
            <Button  variant="outlined"  type="reset" value="Reset" color="primary" onClick={this.handleReset}>Reset </Button>
            </CardActions>
            </Grid>   
            </Fragment>
        </form>
        </Grid> 
          </>
        );
    }
}
}
export default (FilterData);





