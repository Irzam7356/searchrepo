import React from 'react';
import { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
  
function NotificationStatus(props){
        useEffect(() => {
            const { isSuccess, message } = props;
            isSuccess
             ? toast.success(message)
             : toast.success(message)
            console.log("props ", props)
        },[])
        return (
          <div>
            <ToastContainer />
          </div>
        );
    
    }
    
export default NotificationStatus;