 export const DEGREES = { 
    "fname": "Arslan" ,
    "lname": "Ali", 
    "qualifications":[ 
        {"degree": "BCS",
        "institute": "PUCIT",
         "year":"2022",
         "id": "1"
        },
        {"degree": "GCS",
        "institute": "PUCIT",
         "year":"2022",
         "id": "2"
        },
        {"degree": "CSS",
        "institute": "PUCIT",
         "year":"2016",
         "id": "3"
        },
        {"degree" : "ICS",
        "institute": "GCU",
          "year": "2021",
          "id": "4"
        },
        {"degree": "BCS",
        "institute": "COMSATS",
         "year":"2020",
         "id": "5"
        },
        {"degree" : "ICS1",
        "institute": "FAST",
          "year": "2022",
          "id": "6"
        },
        {"degree": "ICOM",
        "institute": "NUST",
         "year":"2015",
         "id": "7"
        },
        {"degree" : "ICS2",
        "institute": "UET",
          "year": "2018",
          "id": "8"
        }
      ]
  }

   export const YEARS = ["2015", "2016", "2017", "2018", "2019" ,"2020" , "2021", "2022"]; 