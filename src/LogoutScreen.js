import React from 'react';
import { compose } from 'recompose';
import { inject, observer } from 'mobx-react';
import { Button, Grid } from '@material-ui/core';

const LogoutScreen = (props) => {
     const logout =() =>{
        props.isLoggedIn(false);
     }
    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            style={{ minHeight: '10vh' }}
        >
            <Button variant="outlined" color="primary" onClick={logout}>Logout</Button>            
        </Grid> 
    );
}
export default compose(
    inject(({
        userDataStore : { removeUserData }
    }
    ) => ({
        removeUserData
    })),
    observer
)(LogoutScreen);







