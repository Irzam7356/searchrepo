import _ from "lodash";

export const mergeArray = ( array1 , array2 ) => {
    const finalArray = [];
    if(array1?.length){
        for(const obj of array1){
            finalArray.push(obj);
        }
    }
    if(array2?.length){
        for(const obj of array2){
            finalArray.push(obj);
        }
    }

    return uniqueArray(finalArray);
}

const uniqueArray = array => {
    return _.uniqBy(array, function (e) { return e.id });
}