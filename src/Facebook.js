import React from 'react';
import FacebookLogin from 'react-facebook-login';
import Mailto from "react-mailto-link";
import NotificationStatus from './NotificationStatus';

class Facebook extends React.Component {
    constructor(props) {   
    super(props);
    this.state = {
    isLoggedIn :false,
    userID :"",
    name :"",
    email:"",
    picture:"",
    toastMessage1:"", toastSuccess1: false , fbData:[]
    };
}
       responseFacebook = (response) => {
        this.setState({fbData:response})
        this.setState({
          isLoggedIn : true,
          userID : response.userID,
          name: response.name,
          email : response.email,
          picture: response.picture.data.url,
        });
    localStorage.setItem('userData', JSON.stringify(this.state.fbData ));
    console.log("localstorage fb" , localStorage)
    this.setState({
      toastMessage1: localStorage?.userData ? "Login with Facebook successfull" : "Login with Facebook not successfull",
      toastSuccess1: localStorage?.userData ? true : false
    })
    this.props.isLoggedIn(true);
  };
  render() {
      let fbContent ; 
       if (this.state.isLoggedIn){
           fbContent = (
             <div style ={{
                 width : '400px',
                 margin: 'auto',
                 background:'#f4f4f4',
                 padding : '20px'
             }}>
                 <img  src={this.state.picture}  alt = {this.state.name} />
                 <br></br>
                  <h2>Welcome !</h2> 
                  <Mailto email="irzam.dawood.7356@gmail.com" obfuscated={true}>{this.state.name}</Mailto>
                 <h3> Email : {this.state.email}</h3> 
             </div>
           );
       } else{
           fbContent =(
           <FacebookLogin
           appId="515733089516372"
           autoLoad={false}
           cookie={false}
           xfbml={false}
           fields="name,email,picture"
           callback={this.responseFacebook}
           icon="fa-facebook"
      />
      

           )
       }
    return (
        <div>
          {fbContent }
          {this.state.toastMessage1 !== "" && <NotificationStatus message={this.state.toastMessage1} isSuccess1={this.state.toastSuccess1} /> }
        </div>
          
    )
  }
}
 
export default Facebook;