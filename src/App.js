import React from 'react';
import FilterData from './FilterData';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import ClickData from './ClickData';
import LogoutScreen from './LogoutScreen';
import { Provider } from 'mobx-react';
import Stores from './Store/rootStore';


 export  default class App extends React.Component{
 
  render(){
    const stores = Stores();
    return(
    <div>
      <Provider {...stores}>
        <Router>
          <Route path='/fitering' exact component={FilterData}/>
          <Route path='/' exact component={ClickData}/>
          <Route path='/logout' exact component={LogoutScreen}/>
        </Router>
      </Provider>
    </div>  
    )
  }
}
