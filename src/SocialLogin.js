import React ,{Component} from 'react';
import Facebook from './Facebook';
import Google from './Google';


class SocialLogin extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <div>
                <Google isLoggedIn={this.props.isLoggedIn}/>
                <Facebook isLoggedIn={this.props.isLoggedIn}/>
            </div>
        );
    }
}

export default SocialLogin;