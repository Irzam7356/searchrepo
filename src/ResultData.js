import React from 'react';
import { DEGREES } from "./data";


 export default class ResultData extends React.Component{
    render(){
        // console.log(  "result from props is " ,  this.props.result) 
        return (
            <div> 
            {this.props.result?.map((object)  => (
                <div key={object.degree}>
                    {console.log("object ", object)}
                    {DEGREES.fname} {" "} {DEGREES.lname} Completed  
                    <span style={{backgroundColor: this.props.result && "blue-white"}} > {object.degree}</span>
                    {" "} from {object.institute} In {" "} 
                    <span style={{backgroundColor: this.props.result &&  "light-red"}} > {" "}{object.year}</span>
                </div>
            ))}
           </div>
        )
    }
}




