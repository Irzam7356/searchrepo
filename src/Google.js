import React  from 'react';
import GoogleLogin from 'react-google-login';
import { compose } from 'recompose';
import { inject, observer } from 'mobx-react';


const Google = (props,) => {
   const responseGoogle = response => {
        if(response?.profileObj){
            props.isLoggedIn(true);
        }
    }
    return (
            <GoogleLogin
                clientId="464122579715-07trc92vpvnsmb94frs7im5rnqccpb3l.apps.googleusercontent.com"
                buttonText= "Login With Google"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={'single_host_origin'}
            />
    );
}
export default compose(
    inject(({
        userDataStore : { setUserData,  }
    }
    ) => ({
        setUserData,
    })),
    observer
)(Google);